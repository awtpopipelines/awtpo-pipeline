package mapper;

import dto.ProductDto;
import javax.annotation.Generated;
import model.Product;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-06-15T23:34:37+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_73 (Oracle Corporation)"
)
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDto productToProductDto(Product source) {
        if ( source == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        return productDto;
    }

    @Override
    public Product productDtoToProduct(ProductDto source) {
        if ( source == null ) {
            return null;
        }

        Product product = new Product();

        return product;
    }
}
